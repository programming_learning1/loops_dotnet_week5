﻿Console.WriteLine();

Console.WriteLine("============================== COURSE_examples_loops ==========================================");

Console.WriteLine("============================== COURSE_example FOR_loops ==========================================");

for (int i = 1; i <= 5; i++)
{
    Console.WriteLine(i);
}


Console.WriteLine("============================ COURSE_example FOREACH_loops ========================================");


int[] array_1 = new int[] { 0, 1, 1, 2, 3, 5, 8, 13 };

foreach (int element in array_1)
{
    Console.WriteLine(element);
}

Console.WriteLine("============================ COURSE_example WHILE_loops ===========================================");

int n = 1;
while (n < 6)
{
    Console.WriteLine("Current value of n is {0}", n);
    n++;
}

Console.WriteLine("============================ COURSE_example DO_WHILE_loops ========================================");

int x = 0;
do
{
    Console.WriteLine(x);
    x++;
} while (x < 5);

Console.WriteLine("========== COURSE_example Breaking-out-of-loops_CONTINUE: go to NEXT_iteration in loop  ===========");

for (int i = 1; i <= 20; i++)
{
    if (i == 7)
    {
        continue;
    }
    Console.WriteLine(i);
}

Console.WriteLine("================== COURSE_example Breaking-out-of-loops_BREAK: exit loop ==========================");

for (int i = 1; i <= 100; i++)
{
    if (i == 11)
    {
        break;
    }
    Console.WriteLine(i);
}

Console.WriteLine("=============== COURSE_example Breaking-out-of-loops_BREAK: exit_METHOD  ==========================");

//for (int i = 1; i <= 100; i++)
//{
//    if (i == 5)
//    {
//        return;
//    }
//    Console.WriteLine(i);
//}

Console.WriteLine("=========== COURSE_example Breaking-out-of-loops_BREAK: exit_METHOD_with_EXCEPTION  ================");

try
{
    for (int i = 1; i <= 100; i++)
    {
        if (i == 5)
        {
            throw new Exception("Something went wrong");
        }
        Console.WriteLine(i);
    }
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}

//for (int i = 1; i <= 100; i++)
//{
//    if (i == 5)
//    {
//        throw new Exception("Something went wrong");
//    }
//    Console.WriteLine(i);
//}



Console.WriteLine();
Console.WriteLine("============================== CLASSROOM_examples_loops ==========================================");
Console.WriteLine("----------------------------- FOR loop ---------------------------------------------------");

Console.WriteLine("before for");
// initialization->[check condition->code execution->incrementation]=>up until condition is not met any more
//for( ; ; ) = it has to be provided with 3 sections! ALWAYS
for (int i = 1; i <= 5; ++i)
{
    Console.WriteLine(i);
}

Console.WriteLine("after for");

for (int i = 5; i >= 0; i--)
{
    Console.WriteLine(i);
}

Console.WriteLine();
int j1 = 1;//initialization outside the for_loop
for (; j1 <= 5; j1++)
{
    Console.WriteLine(j1);
}

Console.WriteLine();
for (int k = 1; ; k++) //condition in the code_block
{
    if (k > 5)
    {
        break;
    }
    Console.WriteLine(k);
}

Console.WriteLine();
for (int q = 1; q <= 5;)//increment in the code_block
{
    Console.WriteLine(q);
    q++;
}

Console.WriteLine();
int m = 1;
for (; ; )//it works, but don't use it in projects, just play with it ;)
{
    if (m > 5)
    {
        break;
    }
    Console.WriteLine(m);
    m++;
}

Console.WriteLine();
List<int> numbersList = new List<int>();
//instead of doing this a lot of times:
//numbers.Add(123);
//numbers.Add(124);
//we can do this:
for (int i = 123; i <= 125; i += 2)
{
    numbersList.Add(i);
}


for (int i = 0; i < numbersList.Count; i++)
{
    Console.WriteLine(numbersList[i]);
}

Console.WriteLine("----------------------------- FOReach loop ---------------------------------------------------");

int[] array_2 = new int[] { 0, 1, 1, 2, 3, 5, 8, 13 };
foreach (int element in array_2)
{
    Console.WriteLine(element);
}


Console.WriteLine();
int[] arrayName1 = new int[] { 23, 45, 657, 12, 129 };
foreach (int element in arrayName1)
{
    Console.WriteLine(element * element);
    Console.WriteLine("another element");
}

Console.WriteLine();
List<string> names = new List<string> { "ana", "maria", "ioan" };
foreach (string name in names)
{
    Console.WriteLine(name);
}

for (int i = 0; i < names.Count; i++)
{
    Console.WriteLine(names[i]);
}

//increment example
Console.WriteLine("++");
int c = 5;
Console.WriteLine(c);
Console.WriteLine(c++);
Console.WriteLine(c);
Console.WriteLine(++c);
Console.WriteLine(c);
        
